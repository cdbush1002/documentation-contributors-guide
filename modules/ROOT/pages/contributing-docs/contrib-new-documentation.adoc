include::ROOT:partial$attributes.adoc[]

= Create a new documentation module
Francois Andrieu, Fedora Documentation Team
2023-04-28

[abstract]
____
This section describes how to add a completely new piece of documentation that covers a new area in its entirety. This spans several pages and is usually associated with the creation of a new, dedicated repository. A local working environment is best suited for this. But it is also possible to use the web IDE of various GIT forges.
____

Before you start following this procedure, review all the requirements listed in xref:contributing-docs/index.adoc#_prerequisites[Prerequisites].


== Documentation repository configuration

NOTE: While you can create a new repository, or use an existing one, we recommend starting from the provided template repository if you are not familiar with Antora.

Create your new repository for the new documentation set, or ask someone to create one for you.
You can host this repository anywhere but we recommend using https://gitlab.com/fedora[GitLab] where you can use Fedora groups to control write access to the repository.
Depending on the topic, it might be preferable to host it under the https://gitlab.com/fedora/docs[Fedora Docs namespace].  

On GitLab, you can use `New project` > `Create from template` > `Group` and pick `Documentation Template` in the list.

If you are not using GitLab, clone the https://gitlab.com/fedora/docs/templates/fedora-docs-template[template repository] manually and copy the content to your new repository.

.Example of a simple documentation repository structure
----
📄 antora.yml
📄 site.yml
📂 modules
  📂 ROOT
    📄 nav.adoc
    📂 pages
      📄 index.adoc
      📄 another-page.adoc
----

In the new repository, edit the `antora.yml` configuration file in the repository root.
The file contains comments that point out which parts you need to change.
At a minimum, always change the `name` and `title`.

NOTE: The `name` is what will define the final URL of your documentation. In example: `docs.fedoraproject.org/en-US/<name>/`

Additionally, edit the `site.yml` configuration file.
Note that this file is only used when building a local preview of your content set - on the website it is overridden by the site-wide `site.yml` configuration.
The only directives you need to edit in this file are the `title` and `start_page`.

At this point, the initial configuration is complete. You can push these changes to the newly created repository (or make a pull request if you do not have the required rights) and start working on writing the actual documentation.

== Writing documentation

Some useful documentation links:

- xref:contributing-docs/asciidoc-markup.adoc[]
- xref:contributing-docs/style-guide.adoc[]

If your documentation is made of several pages, you can list them in the `nav.adoc`. This file will then be used to build the navigation menu on the left side of docs.fp-o.

While you're writing, you can use the xref:contributing-docs/tools-localenv-preview.adoc[local preview] to check the resulting document.

== Publish a new documentation module

Once the repository is set up, and initial content added, it is ready to be published.

Documentation modules published on docs.fp-o are all listed in the https://gitlab.com/fedora/docs/docs-website/docs-fp-o/-/blob/prod/site.yml[main Antora playbook].

To add a new documentation module, you will need to add its repository to the `content.sources` list:

[,yaml]
----
content:
  sources:
  - url: https://gitlab.com/path/to/new/repository.git
    branches: main <.>
    start_path: docs <.>
----
<.> The default branch is set to `master`. If your repository is using any other name (`main` for instance), you need to specify it here.
<.> This setting is optional. If the documentation files are stored in a subdirectory on your repository (`/docs/` for instance), you must set its relative path here, without leading or trailing slashes. If it is located at the root level, as documented on this page, you can omit this parameter.


You can either create a Merge Request with these changes, or if you do not feel comfortable editing this file, create a ticket on the  https://gitlab.com/fedora/docs/docs-website/docs-fp-o/-/issues[Fedora Docs Website repository], and the Documentation Team will handle that part for you.


[NOTE]
====
If you do not get any update to your Merge Request or ticket after 5 days, xref:ROOT:index.adoc#find-docs[get in touch with the Docs Team].
====
